// Globale variabelen
var currentStep = 0;
const maxStep = 4;
const minStep = 0;

const nextStep = document.querySelector('#nextStep');
const previousStep = document.querySelector('#previousStep');

const fastForward = document.querySelector('#fastForward');
const fastBackward = document.querySelector('#fastBackward');


// Kijkt naar huidige stap en bepaalt of knoppen disabled moeten worden of niet
function checkCurrentStep(currentStep) {

	if (this.currentStep !== minStep) {

		previousStep.disabled = false;
		fastBackward.disabled = false;
	} else {
		previousStep.disabled = true;
		fastBackward.disabled = true;
	}

	if (this.currentStep !== maxStep) {
		nextStep.disabled = false;
		fastForward.disabled = false;
	} else {
		nextStep.disabled = true;
		fastForward.disabled = true;
	}
}
//////////////////////////////////////////////////////////////

// Laat stap 0 zien bij page-load
window.addEventListener("load", function () {

	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", ajaxLoad);
	xhr.addEventListener("error", ajaxError);
	xhr.open("GET", "step0.html");
	xhr.send();


	function ajaxLoad(event) {

		if (this.status === 200) {
			var stepDiv = document.querySelector('.steps');
			stepDiv.innerHTML = this.responseText;
		} else {
			console.log("Request niet gelukt");
		}
	}

	function ajaxError(event) {
		console.log('Fout: AJAX');
	}
});


// Generieke functie om request te maken
function createRequest(currentStep) {

	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", ajaxLoad);
	xhr.addEventListener("error", ajaxError);
	xhr.open("GET", "step" + this.currentStep + ".html");
	xhr.send();

	function ajaxLoad(event) {

		if (this.status === 200) {
			var stepDiv = document.querySelector('.steps');
			stepDiv.innerHTML = this.responseText;
			checkCurrentStep(currentStep);
		} else {
			console.log("Request niet gelukt");
		}
	}

	function ajaxError(event) {
		console.log('Fout: AJAX');
	}
}


// Volgende stap
function nextView() {
	if (currentStep != maxStep) {
		currentStep += 1;
		createRequest();
		console.log("Huidige stap: " + currentStep);
	}
}

//vorige stap
function previousView() {
	if (currentStep != minStep) {
		currentStep -= 1;
		createRequest();
		console.log("Huidige stap: " + currentStep);
	}

	//  ZONDER AJAX 
	/*   if (paginanummer!==4){

    document.querySelector('[data-step="'+ paginanummer +'"]').style.display = "none";
    var page = paginanummer+1;

    document.querySelector('[data-step="'+ page +'"]').style.display = "block";
    paginanummer++;

}*/

}

///////EXTRA: Fast forward- and backward ///////////////////
fastForward.addEventListener("click", function () {
	currentStep = 4;
	createRequest(currentStep);
})
/////////////////////////////////////////////////////////////
fastBackward.addEventListener("click", function () {
	currentStep = 0;
	createRequest(currentStep);
})
/////////////////////////////////////////////////////////////





// Update de folder path als de input verandert
function updateFolderPath() {
	document.getElementById('folder').innerHTML = "cd " + document.getElementById('zoekFolderCommando').value;
}

// Update de clone URL als de input verandert
function updateCloneUrl() {
	document.getElementById('cloneCommand').innerHTML = "git clone " + document.getElementById('cloneUrl').value;
}

// Update de commit comments als de input verandert
function updateCommitComments() {
	document.getElementById('commitCommand').innerHTML = 'git commit -a -m "' + document.getElementById('commitComment').value + '"';
}



// Kopieer tekst
function copyCode(nr) {

	var copyCodeArea = document.querySelector('[data-copy="' + nr + '"]').innerHTML;
	document.getElementById("input3").value = copyCodeArea;
	console.log(copyCodeArea);

	copyCodeArea.Select();
	document.execCommand("Copy", false, null);

}